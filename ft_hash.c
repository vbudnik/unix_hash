#include "ft_hash.h"


int main(int ac, char **av)
{

	(void)ac;
	// in main function int structure
	inti_hash_tab();

	// in function when call exex build
	// 
	set_hash_tab("test", hash_tab);

	// in build
	ft_hash(NULL, hash_tab);

	// for test
	char **d = ft_memalloc(2 * sizeof(char *));
	d[0] = strdup(av[1]);
	d[1] = NULL;
	ft_hash(d, hash_tab);
	ft_hash(d, hash_tab);
	ft_hash(NULL, hash_tab);
	set_hash_tab("1 LOLOLO", hash_tab);
	set_hash_tab("1 LOLOLO", hash_tab);
	set_hash_tab("2a", hash_tab);
	ft_hash(NULL, hash_tab);
	ft_hash(d, hash_tab);
	ft_hash(NULL, hash_tab);

	system("leaks a.out");
	return (0);

}

void	inti_hash_tab(void)
{

	if (!(hash_tab = (t_hash_tab *)ft_memalloc(sizeof(t_hash_tab))))
		return ;
	if (!(hash_tab->path = (char **)ft_memalloc(sizeof(char *))))
		return ;
	hash_tab->count = 0;
}

void	set_hash_tab(char *path, t_hash_tab *hash_tab)
{
	int		size_hash_tab;

	if(path == NULL)
	  return ;
	size_hash_tab = 0;
	if (hash_tab->count == 0)
		size_hash_tab = 0;
	else
		size_hash_tab = count_size(hash_tab->path);
	hash_tab->count = (size_hash_tab == 0) ? 1 : size_hash_tab;
	size_hash_tab = (size_hash_tab == 0) ? 1 : size_hash_tab + 1;
	if (!search_add(path, hash_tab))
		return ;
	hash_tab->path = copy_table(path, size_hash_tab, hash_tab);
}

int search_add(char *add, t_hash_tab *hash_tab)
{
	int i;

	i = 0;
	while(hash_tab->path[i] != NULL)
	{
		if (strcmp(hash_tab->path[i], add) == 0)
		{
			return (0);
		}
		i++;
	}
	return (1);
}

char	**copy_table(char *add, int size, t_hash_tab *hash_tab)
{
	char	**tmp;
	int		i;
	int 	j;

	i = 0;
	j = 0;
	if (!(tmp = (char **)ft_memalloc(sizeof(char *) * (size + 1))))
		return (NULL);
	if (hash_tab->count != 0)
	{
		while (hash_tab->path[i] != NULL)
		{
			if (strlen(hash_tab->path[i]) != 0)
			{
				tmp[j] = strdup(hash_tab->path[i]);
				j++;
			}
			i++;
		}
	}
	tmp[j] = strdup(add);
	tmp[j + 1] = NULL;
	free_array(hash_tab->path);
	free(hash_tab->path);
	hash_tab->path = NULL;
	return (tmp);
}



void	free_array(char **array)
{
	int		i;

	i = 0;
	while(array[i] != NULL)
	{
	  	free(array[i]);
	  	array[i] = NULL;
		i++;
	}
	// free(array);
	array = NULL;
}


int		ft_hash(char **hash, t_hash_tab *hash_tab)
{
	if (count_size(hash) > 1)
	{
		printf("use hash or  hash [-d]\n");
	    return (0);
	}
	if (hash != NULL)
	{
		if (strcmp(hash[0], "-d") != 0)
		{
			printf("Incorect flag - [ %s ]\n", hash[0]);
			return (0);
		}
		if (hash_tab->count == 0 && strcmp(hash[0], "-d") == 0)
	  	{
	  		hash_tab->count = 0;
			printf("Hash table is Free\n");
	  		return (1);
	  	}
	    if (strcmp(hash[0], "-d") == 0 || hash_tab->count != 0)
	    {
			hash_tab->count = 0;
			free_array(hash_tab->path);
			printf("Hash table is free\n");
			return (1);
		}
	}
	if (hash_tab->count != 0)
		print_res(hash_tab->path);
	return (1);
}

void	print_res(char **tab)
{
		int max;
		int i;

		i = 0;
		max = max_len(tab);
		printf("<");
		print_table_row(max / 2 + 1, '-');
		printf("Hash Table");
		print_table_row(max / 2 + 1, '-');
		printf(">\n");
		while(tab[i] != NULL)
		{
			printf("|");
			print_table_row(((max - strlen(tab[i])) / 2) + 13 / 2, ' ');
			printf("%s", tab[i]);
			print_table_row(((max - strlen(tab[i])) / 2) + 13 / 2, ' ');
			printf("|\n");
			i++;
		}
		printf("<");
		print_table_row( max + 13, '-');
		printf(">\n");	
}

int		count_size(char **array)
{
	int i;

	i = 0;
	if(!array)
		return (0);
	while(array[i] != NULL)
	{
		i++;
	}
	return (i);
}


int 	max_len(char **array)
{
	int i;
	int max;

	i = 0;
	max = strlen(array[0]);
	while(array[i] != NULL)
	{
		if ((int)strlen(array[i]) > max)
			max = strlen(array[i]);
		i++;
	}
	return (max);
}

void	print_table_row(int size, char symbol)
{
	int i;

	i = 0;
	while(i <= size)
	{
		printf("%c", symbol);
		i++;
	}
}

void	*ft_memalloc(size_t size)
{
	void *str;

	str = malloc(size);
	if (str == NULL)
		return (NULL);
	else
		return (ft_memset(str, 0, size));
}

void	*ft_memset(void *memptr, int val, size_t num)
{
	unsigned char	*s1;

	s1 = memptr;
	while (num)
	{
		*s1++ = (unsigned char)val;
		num--;
	}
	return (memptr);
}
